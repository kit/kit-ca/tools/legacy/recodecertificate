RecodeCertificate
=================

This is a minimal Python script designed to recode a PKCS12 file using OpenSSL.
A Firefox-generated PKCS12 file cannot be imported into Windows under certain
circumstances.  Recoding it with this tool should help.

Windows Binary
==============

A 32-bit stand-alone Windows binary with the script is provided in [the
release notes](https://git.scc.kit.edu/KIT-CA/RecodeCertificate/tags).  To
use the Windows binary, simply double-click the executable file.

If you want to roll the Windows EXE file yourself, you will need a Windows
system with a working Python 3 installation and the `pyinstaller` and the
`pyopenssl` packages installed.  Running `Windows\roll_executable.bat` should
create a working binary in `dist\RecodeCertificate.exe`.

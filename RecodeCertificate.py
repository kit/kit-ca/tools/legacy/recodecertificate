#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from OpenSSL import crypto, SSL 
from os.path import basename, dirname, isfile, splitext
from sys import exit
from tkinter import Button, E, Entry, Label, LEFT, Tk, W, filedialog, messagebox, simpledialog


# Define password query dialog
class _QueryPasswordDialog(simpledialog._QueryDialog):
    def body(self, master):
        w = Label(master, text=self.prompt, justify=LEFT)
        w.grid(row=0, padx=5, sticky=W)

        self.entry = Entry(master, name="entry")
        self.entry.grid(row=1, padx=5, sticky=W+E)

        self.toggle = Button(master, text="⚷", command=self.toggleShow)
        self.toggle.grid(row=1, padx=5, column=1, sticky=W+E)

        if self.initialvalue is not None:
            self.entry.insert(0, self.initialvalue)
            self.entry.select_range(0, END)

        return self.entry

    def toggleShow(self):
        if self.entry["show"] == "":
            self.entry["show"] = "●"
        else:
            self.entry["show"] = ""

class _QueryPassword(_QueryPasswordDialog):
    def __init__(self, *args, **kw):
        if "show" in kw:
            self.__show = kw["show"]
            del kw["show"]
        else:
            self.__show = "●"
        _QueryPasswordDialog.__init__(self, *args, **kw)

    def body(self, master):
        entry = _QueryPasswordDialog.body(self, master)
        if self.__show is not None:
            entry.configure(show=self.__show)
        return entry

    def getresult(self):
        return self.entry.get()

def askpassword(title, prompt, **kw):
    d = _QueryPassword(title, prompt, **kw)
    return d.result


# Create and hide root window
root = Tk()
root.withdraw()


# Read input file
while True:
    infile = filedialog.askopenfilename(title='P12-Eingabedatei',
                                        filetypes=(('PKCS12-Dateien', ('*.p12', '*.P12', '*.pfx', '*.PFX')),
                                                   ('alle Dateien', '*.*')))
    if not(infile):
        messagebox.showinfo('Abbruch', 'Die Dateiauswahl wurde abgebrochen!')
        exit(0)

    # Verify file exists; if so, break
    if isfile(infile):
        break


# Read password
while True:
    password = askpassword('Passworteingabe', 'Passwort des geheimen Schlüssels:')
    if password is None:
        messagebox.showinfo('Abbruch', 'Die Passworteingabe wurde abgebrochen!')
        exit(0)

    # Break the loop if the password decrypts the private key, shout out
    # otherwise
    try:
        with open(infile, 'rb') as pkcs12file:
            p12 = crypto.load_pkcs12(pkcs12file.read(), password.encode('utf-8'))
        break
    except crypto.Error:
        messagebox.showinfo('Fehler!', 'Zertifikat konnte nicht geöffnet werden!')


# Read and write output file
while True:
    outfile = filedialog.asksaveasfilename(title='PKCS12-Ausgabedatei',
                                           initialdir=dirname(infile),
                                           initialfile=splitext(basename(infile))[0]+'-neu.p12',
                                           defaultextension='.p12',
                                           filetypes=(('PKCS12-Dateien', ('*.p12', '*.P12', '*.pfx', '*.PFX')),
                                                      ('alle Dateien', '*.*')))
    if not(outfile):
        messagebox.showinfo('Abbruch', 'Die Dateiauswahl wurde abgebrochen!')
        exit(0)

    try:
        with open(outfile, 'wb') as file:
            file.write(p12.export(password.encode('utf-8')))
        break
    except:
        messagebox.showinfo('Fehler!', 'Kann PKCS12-Datei {} nicht schreiben!'.format(outfile))


# All done
messagebox.showinfo('Zertifikat exportiert!', 'Zertifikat erfolgreich in PKCS12-Datei {} exportiert!'.format(outfile))
